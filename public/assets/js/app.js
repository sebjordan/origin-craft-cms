jQuery(document).ready(function($) {
	
	/*function moreBtn(element, btn, elementHeightToMeasure) {
		$(element).each(function() {
			var elHeight = $(this).find(elementHeightToMeasure).height(),
				elToOpperate = $(this);

			$(this).find(elementHeightToMeasure).height(96);

			$(this).find(btn).on('click', function() {
				if( elToOpperate.find(elementHeightToMeasure).height() == 96 ) {
					elToOpperate.find(elementHeightToMeasure).css('height', elHeight);
					$(this).text('Show less')
				} else {
					elToOpperate.find(elementHeightToMeasure).css('height', 96)
					$(this).text('Show more')
				}
			})
		});		
	}*/

	//moreBtn('.our-team-section .switcher-card', '.show-more-btn', '.desc');

	// Toggle active classes on our business modals

	$('body').on('click', '.modal--links', function() {

		var objLink = $(this);

		$('.modal--links').each(function(index) {

			if (objLink.attr('class') != $(this).attr('class')) {

				$(this).removeClass('uk-active');

			} else {

				objLink.toggleClass('.uk-active');

			}

		});

		var strModalId = $(this).attr('data-modal-id');

		$('.modal--location').each(function(index) {

			if ($(this).attr('id') != strModalId) {

				$(this).removeClass('uk-open');

			}

		});

	});

	// Toggle active classes on our business modal links

	$('body').on('click', '.modal--location .uk-close', function() {

		$('.modal--links').each(function(index) {

			$(this).removeClass('uk-active');

		});

	});

});